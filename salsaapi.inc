<?php
/**
 * @file
 * Helper functions for SalsaDev modules to communicate with the SalsaAPI
 * through the drupal_http_request() function
 */

define('SALSADEV_URL', 'api.salsadev.com');

function salsaapi_index_add($document) {
  
  $xml = '<?xml version="1.0" encoding="UTF-8"?>';
  $xml .= '<indexable uid="' . $document['nid'] . '">';
  
  foreach ($document['contents'] as $cid => $content) {
    $xml .= '<content cid="' . $cid . '" language="' . $content['language'] . '">';
    $xml .= '<![CDATA[' . $content['value'] . ']]>';
    $xml .= '</content>';
  }

  foreach ($document['metadatas'] as $key => $metadata) {
    $xml .= '<metadata key="' . $key . '">' . $metadata . '</metadata>';
  }
  
  $xml .= '</indexable>';

  $params = array(
    'method' => 'POST',
    'content-type' => 'application/xml',
    'url' => 'document',
    'payload' => $xml,
  );
  $response = _salsaapi_call($params);
  if ($response->code == 200) {
    $xml = new SimpleXMLElement($response->data);
  };

}

function salsaapi_lift_keywords($rawtext, $count = 20) {
  $params = array(
    'method' => 'POST',
    'content-type' => 'text/plain',
    'url' => 'lift/keywords',
    'payload' => array(
      'content' => utf8_encode($rawtext),
      'count' => $count,
    ),
  );
  $params['payload'] = http_build_query($params['payload']);
  $keywords = array();
  $reponse = _salsaapi_call($params);
  if ($reponse->code == 200) {
    $xml = new SimpleXMLElement($reponse->data);
    foreach ($xml->tag as $tag) {
      $keywords[] = (string)$tag['term'];
    }
  }
  return $keywords;
}

function salsaapi_search($query, $pagenumber = 0, $pagesize = 20, $type = 'SENSE', $constraints = array(), $facets = array(), $threshold = 0) {
  $xml = '<?xml version="1.0" encoding="UTF-8"?>';
  $xml .= '<searchRequest threshold="' . $threshold . '" ';
  $xml .= 'type="' . $type . '" pageSize="' . $pagesize . '" ';
  $xml .= 'pageNumber="' . $pagenumber . '" constrained="' . (count($constraints) ? 'TRUE' : 'FALSE') . '" ';
  $xml .= 'faceted="' . (count($facets) ? 'TRUE' : 'FALSE') . '">';
  if (count($constraints)) {
    $xml .= '<constraints>';
    foreach ($constraints as $key => $value) {
      $xml .= '<facet key="' . $key . '" value="' . $value . '"/>';
    }
    $xml .= '</constraints>';
  }
  if (count($facets)) {
    $xml .= '<facets>';
    foreach ($facets as $key) {
      $xml .= '<key>' . $key . '</key>';
    }
    $xml .= '</facets>';
  }
  $xml .= '<query>' . $query . '</query>';
  $xml .= '</searchRequest>';
  
  $params = array(
    'method' => 'POST',
    'content-type' => 'application/xml',
    'url' => 'search',
    'payload' => $xml,
  );
  $response = _salsaapi_call($params);
  $results = array();
  $results['total'] = 0;
  if ($response->code == 200) {
    $xml = new SimpleXMLElement($response->data);
    $results['hits'] = array();
    $results['total'] = (int)$xml['total'];
    $results['page'] = (int)$xml['page'];
    if ($results['total'] > 0) {
      foreach ($xml->results->index->hit as $hit) {
        $results['hits'][(string)$hit['uid']] = array(
          'title' => (string)$hit['title'],
          'text' => (string)$hit[0],
        );
      }
    }
  };
  return $results;
}

function salsaapi_account_test() {
  $valid = FALSE;
  $params = array(
    'method' => 'POST',
    'content-type' => 'application/xml',
    'url' => variable_get('salsadev_user', 'demo'),
  );
  $response = _salsaapi_call($params);
  if ($response->code == 200) {
    $valid = TRUE;
  };
  return $valid;
}

function _salsaapi_call($params) {

  $salsadev_user = variable_get('salsadev_user', 'demo');
  $salsadev_password = variable_get('salsadev_password', 'demo2011');
  
  if ($salsadev_user == 'demo' && $params['url'] == 'document') {
    drupal_set_message(t('Warning, it is not safe and allowed to submit on demo account. Subscribe to obtain a free account !'));
    $response = new StdClass();
    $response->code = -1;
    return $response;
  }
  
  $url = 'http://' . $salsadev_user . ':' . $salsadev_password . '@' . SALSADEV_URL;
  $url .= '/' . $params['url'];
  $url .= '?searchbox=' . $salsadev_user;

  $options = array(
    'headers' => array('content-type' => $params['content-type']),
    'method' => $params['method'],
  );
  
  if (isset($params['payload'])) {
    $options['data'] = $params['payload'];
  }

  $response = drupal_http_request($url, $options);
  return $response;
}
