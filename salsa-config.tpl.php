<?php
/**
 * @file
 * The salsa config theme page.
 *
 */
?>
<?php if($form): ?>
<table>
  <tr>
    <td colspan="2">
      <div class="salsalogo">
        <img src="http://doc.salsadev.com/download/attachments/3801505/salsadev.png" />
      </div>
      The SalsaDev module is powered by <a href="http://www.linalis.com">Linalis</a>
    </td>
  </tr>
  <tr style="background: #eee">
    <td><?php print drupal_render($form['salsadev_user']); ?></td>
    <td><?php print drupal_render($form['salsadev_password']); ?></td>
  </tr>
  <tr>
    <td colspan="2" style="font-weight: bold"><?php print drupal_render($form['status']); ?></td>
  </tr>
  <tr>
    <td colspan="2"><?php if (isset($form['notice'])) : ?>
        <div class="messages warning"><?php print drupal_render($form['notice']); ?></div>
      <?php endif; ?>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <?php print drupal_render_children($form); ?>
    </td>
  </tr>
</table>
<?php endif; ?>
